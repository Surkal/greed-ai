import numpy as np


class Grid:
    def __init__(self, height, width, val=9):
        self.height = height
        self.width = width
        self.grid = np.random.randint(1, val+1, size=(height, width))
        self.cursor = np.random.randint(1, min(height, width), size=(2))
        self._init_cursor()
        self.score_max = sum(sum(self.grid))
        self.actions = []

    def _init_cursor(self):
        """
        Replaces the starting position value with 0.
        """
        self.grid[self.cursor[0]][self.cursor[1]] = 0

    @property
    def score(self):
        """
        Returns the score.
        """
        return (self.score_max - sum(sum(self.grid))) / self.score_max

    @property
    def get_actions(self):
        """
        Returns all actions performed on the grid.
        """
        return np.array(self.actions)

    def _check_values(self, direction):
        """
        Verifies that the path of a move from the current
        cursor position is free (not equal to 0).
        """
        i, j = direction[0], direction[1]
        # shorter variable names
        g = self.grid
        pos = self.cursor

        try:
            length = g[pos[0]+i][pos[1]+j]
        except IndexError:
            return None
        if length == 0:
            return None
        if not 0 <= pos[0] + length * i < self.height:
            return None
        if not 0 <= pos[1] + length * j < self.width:
            return None

        if not any(g[pos[0]+i*k][pos[1]+j*k] == 0 for k in range(1, length+1)):
            return True

        return False

    def possible_moves(self):
        """
        Analyze the different possible actions from a position.
        """
        g = self.grid
        pos = self.cursor
        possible_moves = []
        # 4 possible moves (up, down, right, left)
        moves = ((0,1), (-1,0), (0,-1), (1,0))
        for move in moves:
            if self._check_values(move):
                possible_moves.append(move)
        return possible_moves

    def move(self, action):
        i, j = action
        # shorter variable names
        g = self.grid
        pos = self.cursor

        length = g[pos[0]+i][pos[1]+j]
        for k in range(1, length+1):
            g[pos[0]+i*k][pos[1]+j*k] = 0
        # Update the cursor position
        self.cursor = (pos[0]+i*length, pos[1]+j*length)

        # Memorize the action
        self.actions.append(action)
