from random import choice

from grid import Grid


def random_ai(g):
    while True:
        possible_moves = g.possible_moves()
        if not possible_moves:
            break
        g.move(choice(possible_moves))
    return g.score


if __name__ == '__main__':

    g = Grid(20,80)
    print(random_ai(g))
